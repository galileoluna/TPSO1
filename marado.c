#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>


pthread_t th1,th2,th3; //threads

sem_t s1,s2,s3; //semaforos

void t1()
{
    sem_wait(&s1);
    printf("\tma");
    sem_post(&s2);
}
void t2()
{
    sem_wait(&s2);
    printf("\tra");
    sem_post(&s3);
}
void t3()
{
    sem_wait(&s3);
    printf("\tdooo... \n");
    sem_post(&s1);
}
void iniciarSemaforo() //crea los thread y signal al ppthread_join(th1,NULL);rimer semaforo
{
  pthread_create(&th1,NULL,t1,NULL);
  pthread_create(&th2,NULL,t2,NULL);
  pthread_create(&th3,NULL,t3,NULL);

  pthread_join(th1,NULL);
  pthread_join(th2,NULL);
  pthread_join(th3,NULL);


}
int main()
    {
    sem_init(&s1,0,0);
    sem_init(&s2,0,0);
    sem_init(&s3,0,0);


    int cont =  2  ;


    sem_post(&s1);
    for(int i = 0;i<cont*2;i++)
    {


    iniciarSemaforo();

    }

    //faltan sem_destroy
    return 0;
}
