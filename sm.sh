#!/bin/bash
#------------------------------------------------------
# PALETA DE COLORES
#------------------------------------------------------
#setaf para color de letras/setab: color de fondo
    red=`tput setaf 1`;
    green=`tput setaf 2`;
    blue=`tput setaf 4`;
    bg_blue=`tput setab 4`;
    reset=`tput sgr0`;
    bold=`tput setaf bold`;
#------------------------------------------------------
# VARIABLES GLOBALES
#------------------------------------------------------
proyectoActual="/home/galileo/aux/TPSO1";
proyectos="/home/galileo/aux/TPSO1/Repositorios.txt";


#------------------------------------------------------
# DISPLAY MENU
#------------------------------------------------------
imprimir_menu () {
           imprimir_encabezado "\t  S  U  P  E  R  -  M  E  N U ";

        echo -e "\t\t El proyecto actual es:";
        echo -e "\t\t $proyectoActual";

        echo -e "\t\t";
        echo -e "\t\t Opciones:";
        echo "";
        echo -e "\t\t\t a.  Ver estado del proyecto";
        echo -e "\t\t\t b.  Programa instalado";
        echo -e "\t\t\t c.  Buscar archivo";
        echo -e "\t\t\t d.  Buscar String";
        echo -e "\t\t\t e.  Procesos de Calendario ";
        echo -e "\t\t\t g.  Estado de Proyecto";
        echo -e "\t\t\t h.  Actualizar Repositorio ";
        echo -e "\t\t\t i.  Actualizar Carpeta";


        echo -e "\t\t\t q.  Salir";
        echo "";
        echo -e "Escriba la opción y presione ENTER";
}


#------------------------------------------------------
# FUNCTIONES AUXILIARES
#------------------------------------------------------


imprimir_encabezado () {
        clear;
        #Se le agrega formato a la fecha que muestra
        #Se agrega variable $USER para ver que usuario está ejecutando
        echo -e "`date +"%d-%m-%Y %T" `\t\t\t\t\t USERNAME:$USER";
        echo "";
        #Se agregan colores a encabezado
        echo -e "\t\t ${bg_blue} ${red} ${bold}--------------------------------------\t${reset}";
        echo -e "\t\t ${bold}${bg_blue}${red}$1\t\t${reset}";
        echo -e "\t\t ${bg_blue}${red} ${bold} --------------------------------------\t${reset}";
        echo "";
}


esperar () {
        echo "";
        echo -e "Presione enter para continuar";
        read ENTER ;
}


malaEleccion () {
        echo -e "Selección Inválida ..." ;
}

actualizado(){
	if [ -z "$(git status --porcelain)" ]; then 
		echo "Esta actualizado"
		else 
 		beep
		echo "\n NO ESTA SINCRONIZADO"
		git push;

		git pull;
		fi



}

decidir () {
    echo $1;
    while true; do
            echo "desea ejecutar? (s/n)";
                    read respuesta;
                    case $respuesta in
                            [Nn]* ) break;;
                                   [Ss]* ) eval $1
                            break;;
                            * ) echo "Por favor tipear S/s ó N/n.";;
                    esac
    done
}


#------------------------------------------------------
# FUNCTIONES del MENU
#------------------------------------------------------
a_funcion () {
            imprimir_encabezado "\tOpción a.  Ver estado del proyecto";
            decidir "cd $proyectoActual; git status";
}


b_funcion () {

                    imprimir_encabezado "\tOpción b. Ingrese el nombre del programa";
                    read respuesta
                    dpkg -l | grep $respuesta
}


c_funcion () {


                  imprimir_encabezado "\tOpción c.  Ingrese el Path";
                  read directorio
                  cd $directorio
                  echo "Ingrese el nombre del archivo "
                  read archivo

                  echo "Ingrese la extencion "
                  read extencion

                  find *".$extencion" | grep -i $archivo



}


d_funcion () {

  imprimir_encabezado "\tOpción d.  Ingrese el Path";
  read directorio
  cd $directorio
  echo "Ingrese el String que desea buscar "
  read str
  grep -r "$str" >> docs.txt



}




e_funcion () {
    imprimir_encabezado "\tOpción e. Procesos del calendario";
    ps aux | grep calendar

}


g_funcion(){
    imprimir_encabezado "\tOpción g.  Estado del proyecto";
    	decidir "cd $proyectoActual; git status";
	if [ -z "$(git status --porcelain)" ]; then 
		echo "n/ Esta actualizado"
		else 
 		beep
		fi
	
}
h_funcion(){
    imprimir_encabezado "\tOpción h. Actualizar repostorio"

      decidir "cd $proyectoActual; git add -A";
      git add -A;
      git commit -m "Cambio";
      git push;
}
i_funcion(){
    imprimir_encabezado "\tOpción i. Actualizar Carpeta"
	
       decidir "cd $proyectoActual; git pull";
	     git pull;
}

#------------------------------------------------------
# LOGICA PRINCIPAL
#------------------------------------------------------
while  true
do

	
	
        # 1. mostrar el menu
        imprimir_menu;
        # 2. leer la opcion del usuario
        read opcion;
	sincronizado;
        case $opcion in
            a|A) a_funcion;;
            b|B) b_funcion;;
            c|C) c_funcion;;
            d|D) d_funcion;;
            e|E) e_funcion;;
            g|G) g_funcion;;
            h|H) h_funcion;;
  	    i|I) h_funcion;;

            q|Q) break;;
            *) malaEleccion;;
        esac
        esperar;
done
